<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'waitersclubwordpress');

/** MySQL database username */
define('DB_USER', 'waitersclubwordpress_master');

/** MySQL database password */
define('DB_PASSWORD', 'secure_key_waitersclub_wp');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '0KsM8BSX9}#fD,;^sq PQq.n}X!lKwzgI,st*y6Y}@/m,Rgo>PwFf>,R4r_g-iFW');
define('SECURE_AUTH_KEY',  'Vd>b:%~ 9>m1?76*1+&Anvzmj,E~j^:Gf(WUb38qG;@kXGZC82biFj>m[Uwa6m#E');
define('LOGGED_IN_KEY',    'Sp1jLyV(.;b5RRf?^#~ocefGtT}`%HgI9+Q=4&}/75]s:PFMXVW+C|4TGAF6%W-;');
define('NONCE_KEY',        '@R8Z}e8zsajiL-gHsEFV{NDq3;xal}3mR[:3#ufkW&!lNe*s9KWvY8]bF,N)Y~W}');
define('AUTH_SALT',        'a~]9Ei|.v|ogG!Xf6ZZzCR*`?Y|AL[8rg7{8Qh<>6-l%kEvkO?}(z6ZSh];Dls:[');
define('SECURE_AUTH_SALT', '6T<x6wc5tRBw<V<Q-FEb6@F7k_W,P#{CKI]MPM>j(.#;2!*n>p:4:cFn<X>$hM+z');
define('LOGGED_IN_SALT',   ']2Za)(39g0/O$-+ZzF(C4L](ckb(dpVE`K`R}z..S=,o6Ut;Kj;DkR&Q];^/m`6H');
define('NONCE_SALT',       'ld4/oFM<cR- pORqZ;vxW3Y,7#q36fmdcl$l6NAIL.IaXSdk_T9*u;bU.=m)or2y');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
